// Arkadia
#include "ark_core/ark_core.hpp"
// ark_touch
#include "ark_touch/Version.hpp"

//
// Helper Functions
//
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
ark_internal_function void
ShowHelp(ark::String const &help_string)
{
    ark::PrintLn("{}", help_string);
    ark::Exit(0);
}

//------------------------------------------------------------------------------
ark_internal_function void
ShowVersion()
{
    ark::PrintLn(
"{} - {} - stdmatt <stdmatt@pixelwizards.io>\n"
"Copyright (c) {} - stdmatt\n"
"This is a free software (GPLv3) - Share/Hack it\n"
"Check http://stdmatt.com for more :)\n",
    BuildInfo_ark_touch.ProgramName,
    BuildInfo_ark_touch.BuildSimpleVersionString  (),
    BuildInfo_ark_touch.BuildSimpleCopyrightString()
);

    ark::Exit(0);
}

//
// Entry Point
//
//------------------------------------------------------------------------------
i32
ark_main(ark::String const &command_line)
{
    namespace CMD = ark::CMD;
    CMD::Set(command_line);

    CMD::Parser parser;
    CMD::Argument const *help_arg     = parser.CreateArgument("h", "help",    "Show this screen",                CMD::Argument::RequiresNoValues);
    CMD::Argument const *version_arg  = parser.CreateArgument("v", "version", "Show version and copyright info", CMD::Argument::RequiresNoValues);
    CMD::Argument const *verbose_arg  = parser.CreateArgument("V", "verbose", "Show more info output",           CMD::Argument::RequiresNoValues);

    CMD::Parser::ParseResult_t const parse_result = parser.Evaluate();
    if(parse_result.HasFailed()) {
        ark::Fatal(parse_result.GetError().error_msg);
    }
    if(help_arg->WasFound()) {
        ShowHelp(parser.GenerateHelpString());
    } else if(version_arg->WasFound()) {
        ShowVersion();
    }

    ark::Array<ark::String> const &filenames = parser.GetPositionalValues();
    if(filenames.IsEmpty()) {
        ark::Fatal("Missing filenames - Aborting...");
    }

    bool const verbose = verbose_arg->WasFound();
    for(auto const &filename : filenames) {
        ark::String const fullpath = ark::PathUtils::Canonize(filename);
        if(ark::PathUtils::IsFile(fullpath)) {
            u32 const time = ark::DateTime::Now().Timestamp();
            ark::Result<bool> access_result = ark::PathUtils::SetAccessTimeUtc  (fullpath, time);
            ark::Result<bool> modify_result = ark::PathUtils::SetModifiedTimeUtc(fullpath, time);
            if(access_result.HasFailed() || modify_result.HasFailed()) {
                ark::Fatal(
                    "Error while setting times - Path: {} - Error Code: {} - Error Msg: {}",
                    fullpath,
                    access_result.HasFailed() ? access_result.GetError().error_code : modify_result.GetError().error_code,
                    access_result.HasFailed() ? access_result.GetError().error_msg  : modify_result.GetError().error_msg
                );
            }
            if(verbose) {
                ark::Print("Set timestamps for file: ({})", filename);
            }

        } else {
            ark::PathUtils::CreateFileResult_t result = ark::PathUtils::CreateFile(
                fullpath,
                ark::PathUtils::CreateFileOptions::IgnoreIfExists
            );
            if(result.HasFailed()) {
                ark::Fatal(
                    "Error while creating file - Path: {} - Error: {}",
                    fullpath,
                    result.GetError().error_msg
                );
            }
            if(verbose) {
                ark::Print("Created file: ({})", filename);
            }
        }
    }

    return 0;
}
